package at.haj.games.snowflake;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflake {
		 private float x, y;
		 private float size;
		 private float speed;
		 
	public Snowflake(float x, float y, float size, float speed) {
		super();
		this.x = x;
		this.y = y;
		this.size = size;
		this.speed = speed;
	}
	 
	public Snowflake(int size) {
		  super();
		  if (size == 1) {
		   this.size = 10;
		   this.speed = 0.3f;

		 } else if (size == 2) {
			  this.size = 30;
			  this.speed = 0.4f;
			   
		 } else if (size == 3){
		   this.size = 40;
		   this.speed = 0.5f;
		 }
		  setRandomPosition();
		 }

		private void setRandomPosition() {
			 Random random = new Random();
			 this.x = random.nextInt(1500);
			 this.y = random.nextInt(900) - 900;
		}

		public void update(GameContainer gc, int delta) {
			 this.y += delta * this.speed;
			 if (this.y > 900) {
			   setRandomPosition();
			}
		}

		public void render(Graphics graphics) {
			 graphics.fillOval(this.x, this.y, this.size, this.size);
		}
			
	
}

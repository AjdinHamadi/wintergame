package at.haj.games.snowflake;	

//50 gro�e, 50 mittlere und 50 kleinere - die gro�en fallen schneller 	
//Random r = new Random (),      int var = r.nextint(700); 

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;


public class Main extends BasicGame {
	 public List<Snowflake> snowflakes;
	
	 public Main(String title) {
	  super(title);
	 }

	 @Override
	 public void render(GameContainer gc, Graphics graphics) throws SlickException {
	  //zeichnet
	  for (Snowflake snowflake : this.snowflakes) {
		  snowflake.render(graphics); 
	  }
	
	 }

	 @Override
	 public void init(GameContainer gc) throws SlickException {
	  this.snowflakes = new ArrayList<Snowflake>();

	  for (int i = 0; i < 50; i++) {
	   snowflakes.add(new Snowflake(1)); 
	  }
	  
	  for (int i = 0; i < 50; i++) {
	   snowflakes.add(new Snowflake(2)); 
	  }
	  
	  for (int i = 0; i < 50; i++) {
	   snowflakes.add(new Snowflake(3)); 
	  }
	  
	 }

	 @Override
	 public void update(GameContainer gc, int delta) throws SlickException {
	  // delta = zeit seit dem letzten aufruf
	  for (Snowflake snowball: this.snowflakes) {
	   snowball.update(gc, delta);
	  }
	  
	 }

	 public static void main(String[] argv) {
	  try {
	   AppGameContainer container = new AppGameContainer(new Main("Wintergame"));
	   container.setDisplayMode(1500, 900, false);
	   container.start();
	   
	  } catch (SlickException e) {
	   e.printStackTrace();
	  }
	 }
	}
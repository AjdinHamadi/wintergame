package at.haj.games.wintergame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame{
	private int ovalx,ovaly;
	private int rectx, recty;
	private int rrectx, rrecty;
	private int switchvalue = 1;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// zeichnen
		graphics.drawOval(ovalx, ovaly, 100, 100);
		graphics.drawRect(rectx, recty, 100, 100);
		graphics.drawRoundRect(rrectx, rrecty, 100, 100, 20);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// 1 mal aufgerufen
		ovalx = 750;
		ovaly = 500;
		
		rectx = 100;
		recty = 100;
		
		rrectx = 1400;
		rrecty = 800;
	}
	

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf
		ovaly++;
		rectx++;
		rrectx--;
		
		if (ovaly == 1000) {
			ovaly = 0 ;
		}
		
		if(rrectx == 0) {
			rrectx = 1500;
		}
		
		switch (switchvalue) {
	      case 1:
	        rectx++;
	        if (rectx == 1500) {
	          switchvalue++;
	        }
	        break;
	      case 2:
	        recty++;
	        if (recty == 1000) {
	          switchvalue++;
	        }
	        break;
	      case 3:
	        rectx--;
	        if (rectx == 0) {
	          switchvalue++;
	        }
	        break;
	      case 4:
	        recty--;
	        if (recty == 0) {
	          switchvalue = 1;
	        }
	        break;
		}
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(1500,1000,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
}
